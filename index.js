export default class RotationControls {    
    constructor(target, options = {}) {
        this.target = target;
        this.windowInnerWidth = null;
        this.windowInnerHeight = null;
        this.amp = options.amp ?? 1;
        this.lerpFactor = options.lerpFactor != undefined ? options.lerpFactor : .1;
        this.getScreenSize();
        window.addEventListener('resize', this.getScreenSize.bind(this));

        this.mousePosition = {x: 0, y: 0};
        window.addEventListener('pointermove', this.updateMousePosition.bind(this));
    }

    getScreenSize() {
        this.windowInnerWidth = document.documentElement.clientWidth;
        this.windowInnerHeight = window.innerHeight;
    }

    normalizeMousePosition(ev) {
        let normalizedX, normalizedY;
    
        // Desktop
        if (ev.clientX != undefined) {
            normalizedX = -2 * (.5 - ev.clientX / this.windowInnerWidth);
            normalizedY = 2 * (.5 - ev.clientY / this.windowInnerHeight);
        }
        // Mobile 
        else {
            normalizedX = -2 * (.5 - ev.touches[0].clientX / this.windowInnerWidth);
            normalizedY = 2 * (.5 - ev.touches[0].clientY / this.windowInnerHeight);
        }    
        
        return {
            x: normalizedX,
            y: normalizedY
        }
    }
    
    updateMousePosition(ev) {
        this.mousePosition = this.normalizeMousePosition(ev);
    }

    update() {        
        let newX = lerp(this.target.rotation.y, this.mousePosition.x * this.amp, this.lerpFactor);
        let newY = lerp(this.target.rotation.x, -1 * this.mousePosition.y * this.amp, this.lerpFactor);
        
        this.target.rotation.y = newX;
        this.target.rotation.x = newY;
    }
};

function lerp (start, end, amt){
    return (1-amt)*start+amt*end;
}