# three-rotation-controls
Control class that rotates an object according to the position of the mouse.

## Install
```bash
npm i three-rotation-controls
```

## Usage
```javascript
import RotationControls from 'three-rotation-controls'

let controls = new RotationControls(targetObject);

function loop() {
    requestAnimationFrame(loop);
    controls.update();
}
```

## Options

### amp
Default: `1`  
Rotation amplication. A bigger value means the target will rotate more.  
A negative value means the target will rotate in the opposite direction.
```javascript
new RotationControls(targetObject, {
    amp: 1.5
});
```

### lerpFactor
Default: `0.1`  
Interpolation factor. A smaller value means a smoother rotation.
```javascript
new RotationControls(targetObject, {
    lerpFactor: .05
});
```